package htmler

import (
	"encoding/json"
	"net/http"
)

// Scraper scrapes pages
type Scraper struct {
	mercuryServerEndpoint string
}

// NewScraper returns a new Scraper instance with the provided API Key
func NewScraper(mercuryServerEndpoint string) *Scraper {
	var s Scraper
	s.mercuryServerEndpoint = mercuryServerEndpoint
	return &s
}

// ScrapePage returns a string containing the HTML representation of the readability-enabled
// content of the input URL.
func (s Scraper) ScrapePage(url string) (string, error) {
	data, err := s.getMercuryData(url)

	if err != nil {
		return "", err
	}
	return data.Content, nil
}

func (s Scraper) getMercuryData(url string) (MercuryPayload, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", s.mercuryServerEndpoint, nil)
	if err != nil {
		return MercuryPayload{}, err
	}

	req.Header.Add("Mercury-Url", url)
	resp, err := client.Do(req)
	if err != nil {
		return MercuryPayload{}, err
	}

	defer resp.Body.Close()

	var mp MercuryPayload
	jd := json.NewDecoder(resp.Body)
	err = jd.Decode(&mp)
	if err != nil {
		return MercuryPayload{}, err
	}

	return mp, nil
}
