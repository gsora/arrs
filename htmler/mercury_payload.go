package htmler

import "time"

// MercuryPayload contains the output of a Mercury response
type MercuryPayload struct {
	Title         string    `json:"title"`
	Content       string    `json:"content"`
	DatePublished time.Time `json:"date_published"`
	LeadImageURL  string    `json:"lead_image_url"`
	Dek           string    `json:"dek"`
	URL           string    `json:"url"`
	Domain        string    `json:"domain"`
	Excerpt       string    `json:"excerpt"`
	WordCount     int       `json:"word_count"`
	Direction     string    `json:"direction"`
	TotalPages    int       `json:"total_pages"`
	RenderedPages int       `json:"rendered_pages"`
	NextPageURL   string    `json:"next_page_url"`
}
