package config

import (
	"errors"
	"fmt"
	"net/url"

	"github.com/BurntSushi/toml"
)

func ParseFile(path string) (file File, err error) {
	_, err = toml.DecodeFile(path, &file)
	if err != nil {
		return
	}

	if file.BaseURL == "" {
		err = errors.New("base_url is required")
	}

	if file.MercuryServerURL == "" {
		err = errors.New("mercury_server_url is required")
		return
	}

	if file.ListenOn == "" {
		err = errors.New("listen_on is required")
		return
	}

	for index, site := range file.Sites {
		_, siteErr := url.Parse(site.RssURL)
		if siteErr != nil {
			err = fmt.Errorf("error in site #%d, not a valid URL", index+1)
			return
		}

		if site.OutputName == "" {
			err = fmt.Errorf("error in site #%d, missing output_name", index+1)
		}
	}

	if file.Port == 0 {
		file.Port = 8080
	}

	return
}

func (f File) BindString() string {
	return fmt.Sprintf("%s:%d", f.ListenOn, f.Port)
}
