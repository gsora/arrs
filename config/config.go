package config

// File is our Go struct view of a configuration file
type File struct {
	MercuryServerURL string `toml:"mercury_server_url"`
	Port             int    `toml:"port"`
	BaseURL          string `toml:"base_url"`
	ListenOn         string `toml:"listen_on"`
	Sites            []Site `toml:"site"`
}

// Site is a single site to monitor
type Site struct {
	SiteName   string `toml:"site_name"`
	OutputName string `toml:"output_name"`
	RssURL     string `toml:"rss_url"`
}
