module gitlab.com/gsora/arrs

go 1.12

require (
	github.com/BurntSushi/toml v0.3.0
	github.com/gorilla/feeds v1.1.0
	github.com/gorilla/mux v1.7.1
)
