package main

// LayoutElement is a struct holding informations about what RSS sites are available,
// to be used in the small web UI
type LayoutElement struct {
	SiteName    string
	SiteURL     string
	LastUpdated string
}

// Layout is a list of elements
type Layout struct {
	Elements []LayoutElement
	OPMLURL  string
}

const (
	htmlCode string = `
<b>arrs</b>
<ul>
	{{range .Elements}}
	<li><a href="{{.SiteURL}}">{{.SiteName}}</a> - {{.LastUpdated}}</li>
	{{end}}
</ul>
<ul>
	<li><a href="{{.OPMLURL}}">OPML</a></li>
</ul>
`
)
