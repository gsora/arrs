package main

import (
	"flag"
	"html/template"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/gsora/arrs/config"
	"gitlab.com/gsora/arrs/site"
)

var (
	sites                  Layout
	pathsMux               *mux.Router
	startupRoutinesCounter sync.WaitGroup
	configFilePath         string
	siteUpdateSignal       chan site.SiteUpdated
	opml                   OPMLGenerator
)

func main() {
	parseCLIArgs()
	cfg, err := config.ParseFile(configFilePath)
	if err != nil {
		log.Fatal(err)
	}

	pathsMux := mux.NewRouter()
	tmpl := template.New("")
	tmpl, err = tmpl.Parse(htmlCode)
	if err != nil {
		log.Fatal(err)
	}

	pathsMux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		err := tmpl.Execute(w, sites)
		if err != nil {
			log.Fatal(err)
		}
	}).Methods("GET")

	log.Println("Starting arrs!")
	log.Println("Populating RSS endpoints for the first time...")

	// start a goroutine to track site update intervals on the html page
	siteUpdateSignal = make(chan site.SiteUpdated)
	go func() {
		for su := range siteUpdateSignal {
			for i := 0; i < len(sites.Elements); i++ {
				site := &sites.Elements[i]
				if site.SiteName == su.Name {
					site.LastUpdated = su.UpdateStatus
				}
			}
		}
	}()

	// add n sites to the waitgroup
	startupRoutinesCounter.Add(len(cfg.Sites))

	// start a goroutine for each site, and decrease startupRoutinesCounter as soon as
	// it finishes
	for _, rssSite := range cfg.Sites {
		go func(rssSite config.Site) {
			log.Printf("Populating %s...\n", rssSite.SiteName)
			ns, err := site.NewSite(rssSite.RssURL, rssSite.SiteName, cfg.MercuryServerURL, siteUpdateSignal)
			if err != nil {
				log.Fatal(err)
			}

			pathsMux.Handle("/"+rssSite.OutputName, ns)

			log.Println("Registered handler for", rssSite.SiteName, "on /"+rssSite.OutputName)

			l := LayoutElement{
				SiteName:    rssSite.OutputName,
				SiteURL:     cfg.BaseURL + "/" + rssSite.OutputName,
				LastUpdated: "never updated",
			}
			sites.Elements = append(sites.Elements, l)
			startupRoutinesCounter.Done()
		}(rssSite)
	}

	startupRoutinesCounter.Wait()
	log.Println("Finished populating RSS, arrs is up and running on", cfg.BaseURL)

	// register OPML route
	sites.OPMLURL = cfg.BaseURL + "/opml"
	opml = OPMLGenerator{}
	opml.GenerateOPML(sites)
	pathsMux.Handle("/opml", opml)

	srv := &http.Server{
		Handler:      pathsMux,
		Addr:         cfg.BindString(),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	err = srv.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}

func parseCLIArgs() {
	flag.StringVar(&configFilePath, "config", "./config.toml", "configuration file path")
	flag.Parse()
}
