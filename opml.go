package main

import (
	"encoding/xml"
	"net/http"
)

// OPMLGenerator generates an OPML-compliant XML file for a given Layout.
type OPMLGenerator struct {
	OPML       OPML
	opmlString []byte
}

// OPML is the Go struct representing an OPML file.
type OPML struct {
	XMLName xml.Name `xml:"opml"`
	Text    string   `xml:",chardata"`
	Version string   `xml:"version,attr"`
	Body    OPMLBody `xml:"body"`
}

// OPMLBody represents the real data of an OPML document.
type OPMLBody struct {
	Text    string `xml:",chardata"`
	Outline struct {
		Text     string `xml:",chardata"`
		AttrText string `xml:"text,attr"`
		Title    string `xml:"title,attr"`
		Outline  []struct {
			Text   string `xml:",chardata"`
			XMLURL string `xml:"xmlUrl,attr"`
		} `xml:"outline"`
	} `xml:"outline"`
}

// OPMLOutline contains a single URL for an RSS feed.
type OPMLOutline struct {
	Text   string `xml:",chardata"`
	XMLURL string `xml:"xmlUrl,attr"`
}

// GenerateOPML generates an OPML based on l and memorizes it.
func (og *OPMLGenerator) GenerateOPML(l Layout) error {
	var o OPML
	var ob OPMLBody

	o.Version = "2.0"
	ob.Outline.AttrText = "Subscriptions"
	ob.Outline.Title = "Subscriptions"

	for _, ll := range l.Elements {
		var out OPMLOutline
		out.XMLURL = ll.SiteURL
		ob.Outline.Outline = append(ob.Outline.Outline, out)
	}
	o.Body = ob

	data, err := xml.Marshal(o)
	if err != nil {
		return err
	}

	og.opmlString = data
	return nil
}

func (og OPMLGenerator) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/x-opml")
	w.Write([]byte(og.opmlString))
	return

}
