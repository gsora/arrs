# arrs: an RSS proxy

Proxies your favorites RSS feeds with complete posts content, courtesy of [Mercury](http://mercury.postlight.com/).

Use with caution.

Needs [mercury-http-wrapper](https://gitlab.com/gsora/mercury-http-wrapper) to work.

See `example.toml` for more informations about how to configure it.

---

Deploy it as you wish.

Supports reverse proxy settings via nginx:

```
location ^~ /rss/ {
	proxy_pass http://127.0.0.1:6969/;
}
```

This will assume `arrs` runs on 127.0.0.1:6969, and you'll be able to reach it from the outside on `your_domain/rss`
