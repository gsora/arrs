package site

import (
	"encoding/xml"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/feeds"
	"gitlab.com/gsora/arrs/htmler"
)

const (
	updateTimeFmt string = "Mon Jan _2 15:04:05 2006"
)

type Site struct {
	RSSUrl             string
	RSS                *feeds.Feed
	originalRSS        *RSS
	updateInterval     time.Duration
	Name               string
	scraper            *htmler.Scraper
	updateTicker       <-chan time.Time
	masterUpdateSignal chan SiteUpdated
	updating           bool
}

type SiteUpdated struct {
	Name         string
	UpdateStatus string
}

func (s Site) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	rss, err := s.RSS.ToRss()
	if err != nil {
		w.Write([]byte(fmt.Sprintln("error while decoding RSS xml:", err.Error())))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/rss+xml")
	w.Write([]byte(rss))
	return

}

func (s Site) sendUpdateStatus() {
	s.masterUpdateSignal <- s.updateStatusGen()
}

func (s Site) updateStatusGen() SiteUpdated {
	us := fmt.Sprintf("last updated: %s\n", time.Now().Format(updateTimeFmt))
	if s.updating {
		us = "updating..."
	}
	return SiteUpdated{
		Name:         s.Name,
		UpdateStatus: us,
	}
}

func NewSite(rssUrl string, name string, mercuryServerEndpoint string, masterUpdateSignal chan SiteUpdated) (*Site, error) {
	var s Site
	s.RSSUrl = rssUrl
	s.Name = name
	s.masterUpdateSignal = masterUpdateSignal

	// download a fresh copy of the original RSS
	err := s.downloadOriginalRSS()
	if err != nil {
		return &Site{}, err
	}

	// detect and set the global update interval
	s.updateInterval = s.originalRSS.UpdateDuration()

	s.RSS = &feeds.Feed{
		Title:       s.originalRSS.Title,
		Link:        &feeds.Link{Href: s.originalRSS.Link},
		Description: s.originalRSS.Description,
	}

	s.scraper = htmler.NewScraper(mercuryServerEndpoint)

	// call update for the first time, then start the background
	// rss update goroutine
	err = s.updateRSS()
	if err != nil {
		return nil, err
	}

	go func(site *Site) {
		for {
			select {
			case <-site.updateTicker:
				log.Println("updating", site.Name)
				site.updating = true
				s.sendUpdateStatus()
				err := site.updateRSS()
				if err != nil {
					log.Println("encountered error while updating:", err)
					site.updating = false
					continue
				}
				log.Println("done updating", site.Name)
				site.updating = false
				s.sendUpdateStatus()
			}
		}

	}(&s)

	return &s, nil
}

func (s *Site) updateRSS() error {
	defer func() {
		s.updateTicker = time.After(s.updateInterval)
	}()

	err := s.downloadOriginalRSS()
	if err != nil {
		// log the error
		log.Println(err)
		return err
	}

	err = s.processOriginalRSS()
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (s *Site) downloadOriginalRSS() error {
	rssXMLReq, err := http.Get(s.RSSUrl)
	if err != nil {
		return err
	}

	defer rssXMLReq.Body.Close()

	xmlDecoder := xml.NewDecoder(rssXMLReq.Body)
	var newData RSS
	err = xmlDecoder.Decode(&newData)
	if err != nil {
		return err
	}

	s.originalRSS = &newData

	return nil
}

func (s *Site) processOriginalRSS() error {
	// Encoded == content
	// DescriptionItemChannel == article description
	// TItleChannel == titles
	// LinkChannel.Href == article link
	// PubDate == publication date

	s.RSS.Items = []*feeds.Item{}
	for i := 0; i < len(s.originalRSS.DescriptionItemChannel); i++ {
		// parsing date first
		t, err := time.Parse("Mon, 2 Jan 2006 15:04:05 -0700", s.originalRSS.PubDate[i])
		if err != nil {
			// just set the date to time.Now()
			t = time.Now()
		}

		// get the readability content
		var content string
		content, err = s.scraper.ScrapePage(s.originalRSS.LinkItemChannel[i])
		if err != nil {
			// if we cannot get the scraped content, just use the short one
			content = s.originalRSS.Encoded[i]

			// and log the error
			log.Println(err)
		}

		item := feeds.Item{
			Title:       s.originalRSS.TitleItemChannel[i],
			Link:        &feeds.Link{Href: s.originalRSS.LinkItemChannel[i]},
			Description: s.originalRSS.DescriptionItemChannel[i],
			Created:     t,
			Content:     content,
		}
		s.RSS.Items = append(s.RSS.Items, &item)
	}

	return nil
}
