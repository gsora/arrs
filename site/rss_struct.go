package site

import (
	"strconv"
	"time"
)

type RSS struct {
	Height                 string        `xml:"channel>image>height"`
	Atom                   string        `xml:"atom,attr"`
	Generator              string        `xml:"channel>generator"`
	Url                    string        `xml:"channel>image>url"`
	Category               [][]string    `xml:"channel>item>category"`
	TitleChannel           string        `xml:"channel>title"`
	Width                  string        `xml:"channel>image>width"`
	PubDate                []string      `xml:"channel>item>pubDate"`
	DescriptionItemChannel []string      `xml:"channel>item>description"`
	Guid                   []Guid        `xml:"channel>item>guid"`
	LinkChannel            []LinkChannel `xml:"channel>link"`
	Sy                     string        `xml:"sy,attr"`
	Description            string        `xml:"channel>description"`
	Title                  string        `xml:"channel>image>title"`
	TitleItemChannel       []string      `xml:"channel>item>title"`
	LinkItemChannel        []string      `xml:"channel>item>link"`
	Creator                []string      `xml:"channel>item>creator"`
	Language               string        `xml:"channel>language"`
	Wfw                    string        `xml:"wfw,attr"`
	UpdatePeriod           string        `xml:"channel>updatePeriod"`
	Info                   Info          `xml:"channel>info"`
	UpdateFrequency        string        `xml:"channel>updateFrequency"`
	Content                string        `xml:"content,attr"`
	Dc                     string        `xml:"dc,attr"`
	Slash                  string        `xml:"slash,attr"`
	Link                   string        `xml:"channel>image>link"`
	LastBuildDate          string        `xml:"channel>lastBuildDate"`
	Encoded                []string      `xml:"channel>item>encoded"`
	Version                string        `xml:"version,attr"`
}

type Info struct {
	Feedburner string `xml:"feedburner,attr"`
	Uri        string `xml:"uri,attr"`
}
type Guid struct {
	IsPermaLink string `xml:"isPermaLink,attr"`
	Text        string `xml:",chardata"`
}
type LinkChannel struct {
	Type   string `xml:"type,attr"`
	Href   string `xml:"href,attr"`
	Atom10 string `xml:"atom10,attr"`
	Rel    string `xml:"rel,attr"`
}

func (r RSS) UpdateDuration() time.Duration {
	freq, _ := strconv.Atoi(r.UpdateFrequency)
	// parse UpdatePeriod
	// <sy:updatePeriod> ( 'hourly' | 'daily' | 'weekly' | 'monthly' | 'yearly' )

	switch r.UpdatePeriod {
	case "hourly":
		return time.Duration(freq) * time.Hour
	case "daily":
		return time.Duration(freq) * (24 * time.Hour)
	case "weekly":
		return time.Duration(freq) * (24 * 7 * time.Hour)
	case "monthly":
		return time.Duration(freq) * (24 * 7 * 30 * time.Hour)
	case "yearly":
		return time.Duration(freq) * (24 * 7 * 30 * 365 * time.Hour)
	}

	return 1 * time.Hour

}
